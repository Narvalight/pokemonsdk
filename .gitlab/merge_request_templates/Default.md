### MR Description
- Description

### Before testing
- Thing to do

### Notes to acknowledge before testing
- Thing to acknowledge

### Tests to realize
- [ ] First test

<!-- if appliable !-->
### Showcasing content
<!-- [image showcasing](url_image.png) !-->
<!-- [video showcasing](url_video.mp4) !-->

<!-- if appliable !-->
### Sources related to this MR
- [Bulbapedia link](https://bulbapedia.bulbagarden.net)
- [Pokepedia link](https://www.pokepedia.fr)
- [Discord link of the support](https://ptb.discord.com)
- [Discord link of the dev topic](https://ptb.discord.com)
- [ClickUp link of the ticket](https://app.clickup.com)
